Source: lgeneral
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Markus Koschany <apo@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libsdl-mixer1.2-dev,
 libsdl1.2-dev
Standards-Version: 4.5.1
Homepage: http://lgames.sourceforge.net
Vcs-Git: https://salsa.debian.org/games-team/lgeneral.git
Vcs-Browser: https://salsa.debian.org/games-team/lgeneral

Package: lgeneral
Architecture: any
Depends:
 lgc-pg,
 lgeneral-data,
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 game-data-packager
Description: strategy game in the tradition of Panzer General
 LGeneral is a turn-based strategy game heavily inspired by Panzer General.
 You play single scenarios or whole campaigns turn by turn against a human
 player or the AI.
 .
 Entrenchment, rugged defense, defensive fire, surprise contacts, surrender,
 unit supply, weather influence, reinforcements and other implementations
 contribute to the tactical and strategic depth of the game.
 .
 This is the game engine only. LGeneral requires lgeneral-data or lgc-pg, which
 converts the game data of the original Panzer General game, to function
 properly.

Package: lgc-pg
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 lgeneral
Description: LGeneral converter for Panzer General game data
 LGeneral is a turn-based strategy game heavily inspired by Panzer General.
 .
 This package provides lgc-pg a tool to convert the original game data of
 Panzer General to LGeneral's native file format.
